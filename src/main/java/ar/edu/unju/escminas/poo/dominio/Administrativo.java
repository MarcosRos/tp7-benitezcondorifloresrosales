package ar.edu.unju.escminas.poo.dominio;

import ar.edu.unju.escminas.poo.tablas.TablaOrganismos;
import ar.edu.unju.escminas.poo.tablas.TablaParques;
import ar.edu.unju.escminas.poo.tablas.TablaProvincias;

public class Administrativo extends Usuario {
	public Administrativo() {
		// TODO Auto-generated constructor stub
	}

	public Administrativo(int codigo, String usuario, String contraseņa, String nombre, String apellido, int dni,
			String tipoUsuario) {
		super(codigo, usuario, contraseņa, nombre, apellido, dni, tipoUsuario);
		// TODO Auto-generated constructor stub
	}

	public void darDeAltaParque(Parque unParque) {
		TablaParques.parques.add(unParque);
	}
	
	public void darDeAltaProvincia(Provincia unaProvincia) {
		TablaProvincias.provincias.add(unaProvincia);
	}
	
	public void darDeAltaOrganismo(OrganismoProvincial unOrganismo) {
		TablaOrganismos.organismos.add(unOrganismo);
	}

	@Override
	public String toString() {
		return "Administrativo [toString()=" + super.toString() + "]";
	}
	
	
}
