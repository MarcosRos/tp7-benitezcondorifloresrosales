package ar.edu.unju.escminas.poo.principal;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import ar.edu.unju.escminas.poo.dominio.OrganismoProvincial;
import ar.edu.unju.escminas.poo.dominio.Parque;
import ar.edu.unju.escminas.poo.dominio.Provincia;
import ar.edu.unju.escminas.poo.dominio.Usuario;
import ar.edu.unju.escminas.poo.dominio.Vegetal;
import ar.edu.unju.escminas.poo.dominio.Administrativo;
import ar.edu.unju.escminas.poo.dominio.Secretario;
import ar.edu.unju.escminas.poo.tablas.TablaMinerales;
import ar.edu.unju.escminas.poo.tablas.TablaAnimales;
import ar.edu.unju.escminas.poo.tablas.TablaVegetales;
import ar.edu.unju.escminas.poo.tablas.TablaOrganismos;
import ar.edu.unju.escminas.poo.tablas.TablaParques;
import ar.edu.unju.escminas.poo.tablas.TablaProvincias;
import ar.edu.unju.escminas.poo.tablas.TablaUsuarios;
import ar.edu.unju.escminas.poo.dominio.Animal;
import ar.edu.unju.escminas.poo.dominio.Especie;
import ar.edu.unju.escminas.poo.dominio.Mineral;

public class Principal {

	public static void main(String[] args) {

		Scanner lectorMain = new Scanner(System.in);

		// CARGAMOS UNOS CUANTOS USUARIOS//
		TablaUsuarios.usuarios.add(new Usuario(1, "jperez", "123", "Juan", "Perez", 268956287, "Administrativo"));
		TablaUsuarios.usuarios.add(new Usuario(1, "pgonzalez", "456", "Patricia", "Gomez", 26972234, "Administrativo"));
		TablaUsuarios.usuarios.add(new Usuario(1, "mdiaz", "789", "Mar�a", "Diaz", 28654447, "Secretario"));
		
		

		// CARGAMOS UNAS CUANTAS PROVINCIAS
		TablaProvincias.provincias.add(new Provincia("JU", "Jujuy", new ArrayList<Parque>()));
		TablaProvincias.provincias.add(new Provincia("SA", "Salta", new ArrayList<Parque>()));
		TablaProvincias.provincias.add(new Provincia("BA", "Buenos Aires", new ArrayList<Parque>()));
		TablaProvincias.provincias.add(new Provincia("FO", "Formosa", new ArrayList<Parque>()));
		TablaProvincias.provincias.add(new Provincia("CO", "Cordoba", new ArrayList<Parque>()));

		// ORGANISMOS
		TablaOrganismos.organismos.add(new OrganismoProvincial("Secretaria Espacio Verde Jujuy", "Belgrano 1150",
				"Jose Estrada", TablaProvincias.provincias.get(0)));
		TablaOrganismos.organismos.add(new OrganismoProvincial("Departamento de Turismo Salta", "Senador Perez 233",
				"Mario Serapio", TablaProvincias.provincias.get(1)));
		TablaOrganismos.organismos.add(new OrganismoProvincial("Administracion de Parques Nacionales BS.AS.",
				"Humahuaca 150", "Daniel Rioja", TablaProvincias.provincias.get(2)));
		TablaOrganismos.organismos.add(new OrganismoProvincial("Secretaria de cuidados de Formosa",
				"Sargento Cabral 567", "Ezequiel Zalazar", TablaProvincias.provincias.get(3)));
		TablaOrganismos.organismos.add(new OrganismoProvincial("Organismo Autonomo de Cordoba", "Sarmiento 1296",
				"Martin Almeda", TablaProvincias.provincias.get(4)));

		// ESPECIES parque cordoba
		List<Especie> especiesParqueCordoba = new ArrayList<Especie>();
		especiesParqueCordoba.add(new Vegetal(0, "Bryophyta Schimp", "musgos", false, "no tiene"));
		especiesParqueCordoba.add(new Vegetal(1, "Polypodiophyta", "algas", false, "no tiene"));
		especiesParqueCordoba.add(new Vegetal(2, "Leguminoceae-Fabaceae", "helechos", true, "primavera"));
		especiesParqueCordoba.add(new Animal(0, "Phantera leo", "leon", "carnivoro", "invierno", false));
		especiesParqueCordoba
				.add(new Animal(1, "Panthera tigris tigris", "tigre de vengala", "carnivoro", "Oto�o", true));
		especiesParqueCordoba.add(new Mineral(0, "Oro", "oro", "Metalico"));
		especiesParqueCordoba.add(new Mineral(1, "Carbon", "carbon", "No Metalico"));

		// ESPECIES PARQUE JUJUY
		List<Especie> especiesParqueJujuy = new ArrayList<Especie>();
		especiesParqueJujuy.add(new Vegetal(0, "Arecaceae", "acacia", true, "invierno"));
		especiesParqueJujuy.add(new Vegetal(1, "Cyperus papyrus", "palmeras", false, "primavera"));
		especiesParqueJujuy.add(new Animal(0, "Ramphastos toco", "tucan", "herbivoro", "invierno", true));
		especiesParqueJujuy.add(new Mineral(0, "Plata", "plata", "Metalico"));
		especiesParqueJujuy.add(new Mineral(1, "Diamante", "diamante", "No Metalico"));

		// ESPECIES PARQUE SALTA
		List<Especie> especiesParqueSalta = new ArrayList<Especie>();
		especiesParqueSalta.add(new Vegetal(112, "Polypodiophyta", "algas", false, "no tiene"));
		especiesParqueSalta.add(new Vegetal(113, "Leguminoceae-Fabaceae", "helechos", true, "primavera"));
		especiesParqueSalta.add(new Animal(254, "Phantera leo", "leon", "carnivoro", "invierno", false));
		especiesParqueSalta.add(
				new Animal(367, "Gorilla beringei beringei", "gorila lomo plateado", "herviboro", "invierno", true));
		especiesParqueSalta.add(new Mineral(635, "Cobre", "cobre", "Metalico"));
		especiesParqueSalta.add(new Mineral(455, "Bismuto", "bismuto", "Semi Metal"));

		// ESPECIES PARQUE FORMOSA
		List<Especie> especiesParqueFormosa = new ArrayList<Especie>();
		especiesParqueFormosa.add(new Vegetal(0, "Orchidaceae", "orquideas", false, "no tiene"));
		especiesParqueFormosa.add(new Vegetal(1, "zingiber officinale", "jengibre", false, "no tiene"));
		especiesParqueFormosa
				.add(new Animal(0, "Gorilla beringei beringei", "gorila lomo plateado", "herviboro", "invierno", true));
		especiesParqueFormosa.add(new Animal(0, "Pan troglodytes ", "chimpance", "herviboro", "invierno", true));
		especiesParqueFormosa.add(new Mineral(0, "Platino", "platino", "Platino"));

		// ESPECIES PARQUE BUENOS AIRES
		List<Especie> especiesParqueBuenosAires = new ArrayList<Especie>();
		especiesParqueBuenosAires.add(new Vegetal(2, "Orchidaceae", "orquideas", false, "no tiene"));
		especiesParqueBuenosAires.add(new Vegetal(1, "Leguminoceae-Fabaceae", "helechos", true, "primavera"));
		especiesParqueBuenosAires.add(new Animal(0, "Ambytoma mexicanum", "ajolote", "carnivoro", "verano", true));
		especiesParqueBuenosAires.add(new Mineral(0, "Hierro", "hierro", "Hierro"));

		// PARQUES
		TablaParques.parques
				.add(new Parque(1008, "Parque Nacional Quebrada del Condorito", LocalDate.parse("2005-10-12"), 89706,
						"Cordoba", TablaOrganismos.organismos.get(4), especiesParqueCordoba));
		TablaParques.parques.add(new Parque(1470, "Monumento Natural Laguna de Pozuelos", LocalDate.parse("2018-05-14"),
				16000, "Jujuy", TablaOrganismos.organismos.get(0), especiesParqueJujuy));
		TablaParques.parques.add(new Parque(1057, "Parque Nacional Baritu", LocalDate.parse("1995-12-23"), 19008,
				"Salta", TablaOrganismos.organismos.get(1), especiesParqueSalta));
		TablaParques.parques.add(new Parque(3112, "Parque Nacional Rio Pilcomayo", LocalDate.parse("2010-05-30"), 80636,
				"Formosa", TablaOrganismos.organismos.get(3), especiesParqueFormosa));
		TablaParques.parques
				.add(new Parque(5645, "Parque Nacional Ciervo de los Pantanos", LocalDate.parse("2020-06-02"), 14336,
						"Buenos Aires", TablaOrganismos.organismos.get(2), especiesParqueBuenosAires));

		TablaProvincias.provincias.get(0).setParques(TablaParques.parques);
		TablaProvincias.provincias.get(1).setParques(TablaParques.parques);
		TablaProvincias.provincias.get(2).setParques(TablaParques.parques);
		TablaProvincias.provincias.get(3).setParques(TablaParques.parques);
		TablaProvincias.provincias.get(4).setParques(TablaParques.parques);

		// LOGGEO
		int cantidadUsuarios = TablaUsuarios.usuarios.size();
		boolean loggeado = false;
		int posicionEncontrado;

		do {
			System.out.print("Usuario: ");
			String u = lectorMain.next();
			System.out.print("Contrase�a: ");
			String c = lectorMain.next();

			for (posicionEncontrado = 0; posicionEncontrado < cantidadUsuarios
					&& loggeado == false; posicionEncontrado++) {
				if (u.equals(TablaUsuarios.usuarios.get(posicionEncontrado).getUsuario())
						&& (c.equals(TablaUsuarios.usuarios.get(posicionEncontrado).getContrase�a()))) {
					loggeado = true;
				}

			}
			posicionEncontrado--;
			if (loggeado == true) {
				System.out.println("USUARIO LOGGEADO!" + "\n");
				if (TablaUsuarios.usuarios.get(posicionEncontrado).getTipoUsuario().equals("Administrativo")) {
					System.out.println(
							"ADMINISTRATIVO: " + TablaUsuarios.usuarios.get(posicionEncontrado).getUsuario() + "\n");
					System.out.println(LocalDate.now());
					Administrativo unAdministrativo= (Administrativo) TablaUsuarios.usuarios.get(posicionEncontrado);
					menuAdministrativo(unAdministrativo);
					loggeado = false;
				} else {

					System.out.println(
							"SECRETARIO: " + TablaUsuarios.usuarios.get(posicionEncontrado).getUsuario() + "\n");
					System.out.println(LocalDate.now());
					Secretario unSecretario= (Secretario) TablaUsuarios.usuarios.get(posicionEncontrado);
					menuSecretario(unSecretario);
					loggeado = false;
				}
			} else
				System.out.println("Usuario o contrase�a incorrecta. Vuelva a intentar" + "\n");

		} while (loggeado == false);
		System.out.println("Cerrando el programa");
		lectorMain.close();
	}

	// PROCEDIMIENTO menu secretario
	public static void menuSecretario(Secretario unSecretario) {

		Scanner lectorSecretario = new Scanner(System.in);
		int opc1 = 0;
		boolean banderaMenu = true;
		do {
			do {
				try {
					banderaMenu = true;
					System.out.println("1.Lista de parques por provincias");
					System.out.println("2.Listas de especies"); // mostrar sus atributos y cantidad por cada especie
					System.out.println("3.Listas de parques con mas de 10 a�os");
					System.out.println("4.Listas de organismos ordenados alfabeticamente");
					System.out.println("5.Salir");
					opc1 = Integer.parseInt(lectorSecretario.next());
				} catch (NumberFormatException nfe) {
					System.out.println("El valor ingresado no corresponde al de un numero entero");
					banderaMenu = false;
				}
			} while (banderaMenu == false);

			switch (opc1) {
			case 1:
				Scanner lector2 = new Scanner(System.in);

				System.out.println("Ingrese nombre de la provincia");
				String nombreProvincia = lector2.next();
				
				unSecretario.listarParquesPorProvincia(nombreProvincia);

				break;

			case 2:
				Scanner lector3 = new Scanner(System.in);
				Integer nombreParque = 0;
				Boolean bandera = true;
				do {
					try {
						bandera = true;
						System.out.println("Ingrese codigo del parque");
						nombreParque = Integer.parseInt(lector3.nextLine());
					} catch (NumberFormatException nfe) {
						bandera = false;
						System.out.println("El valor ingresado no corresponde al formato");
					}
				} while (bandera == false);

				unSecretario.listarEspecies(nombreParque);
				
				break;
			case 3:
				unSecretario.listarParques10Anios();
				
				break;

			case 4:
				unSecretario.listarOrganismosAlfabeticamente();
				
				break;

			}
		} while (opc1 != 5);
		lectorSecretario.close();
	}

	// procedimiento menu administrativo
	public static void menuAdministrativo(Administrativo unAdministrativo) {
		Scanner lectorAdministrativo = new Scanner(System.in);

		int opc = 0, auxInt = 0;

		Parque auxParque = new Parque();
		Provincia auxProvincia = new Provincia();
		boolean banderaMenu = true;
		OrganismoProvincial auxOrganismo = new OrganismoProvincial();
		Especie auxEspecie = new Especie();

		do {
			do {
				try {
					banderaMenu = true;
					System.out.println("1. Alta de parques");
					System.out.println("2. Alta de provincias");
					System.out.println("3. Alta de organismos");
					System.out.println("4. salir");

					opc = Integer.parseInt(lectorAdministrativo.next());
				} catch (NumberFormatException e) {
					System.out.println("El valor ingresado no corresponde al tipo entero");
					banderaMenu = false;
				}
			} while (banderaMenu == false);

			switch (opc) {
			case 1:
				Scanner lector2 = new Scanner(System.in);
				boolean bandera = true;
				do {
					bandera = true;
					System.out.println("Ingrese codigo del parque");
					try {
						auxInt = lector2.nextInt();
					} catch (InputMismatchException ime) {
						System.out.println("Ingrese un valor de tipo entero!");
						bandera = false;
						lector2.next();
					}
				} while (bandera == false);
				auxParque.setCodigo(auxInt);
				lector2.nextLine();
				System.out.println("Ingrese nombre del parque");
				auxParque.setNombre(lector2.nextLine());

				LocalDate testDate = null;

				do {
					try {
						System.out.println("Ingrese fecha de inaguracion (Formato dd/MM/YYYY)");
						String fecha = lector2.nextLine();
						bandera = true;
						testDate = convertirStringLocalDate(fecha);
					} catch (Exception e) {
						System.out.println(e.getMessage());
						bandera = false;
					}
				} while (bandera == false);

				auxParque.setFecha(testDate);

				do {
					bandera = true;
					System.out.println("Ingrese extension del parque");
					try {
						auxInt = lector2.nextInt();
					} catch (InputMismatchException ime) {
						System.out.println("Ingrese un valor de tipo entero!");
						bandera = false;
						lector2.next();
					}
				} while (bandera == false);
				auxParque.setExtension(auxInt);

				lector2.nextLine();

				String auxProv;
				do {
					System.out.println("Ingrese ubicacion geografica");
					auxProv = lector2.nextLine();
					if (recorrerProvincias(auxProv) == false) {
						System.out.println("Ubicacion invalida");
						bandera = false;
					} else
						bandera = true;
				} while (bandera == false);

				auxParque.setUbicacionGeografica(auxProv);

				System.out.println("Desea agregar especies? (si/no) ");
				String respuesta = lector2.nextLine();
				while (respuesta.equalsIgnoreCase("si")) {
					int respuesta2 = 0;
					do {
						do {
							bandera = true;
							System.out
									.println("Que especie quiere agregar?" + "\n 1. Animal \n 2. Vegetal \n 3.Mineral");
							try {
								auxInt = lector2.nextInt();
							} catch (InputMismatchException ime) {
								System.out.println("Ingrese un valor de tipo entero!");
								bandera = false;
								lector2.next();
							}
						} while (bandera == false);
						respuesta2 = auxInt;

						if (respuesta2 == 1) {
							auxEspecie = new Animal();

						}
						if (respuesta2 == 2) {
							auxEspecie = new Vegetal();

						}
						if (respuesta2 == 3) {
							auxEspecie = new Mineral();

						}
						do {
							do {
								bandera = true;
								System.out.println("Ingrese codigo de la especie");
								try {
									auxInt = lector2.nextInt();
								} catch (InputMismatchException ime) {
									System.out.println("Ingrese un valor de tipo entero!");
									bandera = false;
									lector2.next();
								}
							} while (bandera == false);

							if ((buscarMineral(auxInt) == true) || (buscarAnimal(auxInt) == true)
									|| (buscarVegetal(auxInt) == true)) {
								System.out.println("Ya existe una especie con dicho codigo");
								bandera = false;
							} else {
								auxEspecie.setCodigo(auxInt);
								bandera = true;
							}
						} while (bandera == false);

						lector2.nextLine();

						System.out.println("Ingrese denominacion cientifica de la especie");
						auxEspecie.setDenominacionCientifica(lector2.nextLine());
						System.out.println("Ingrese denominacion vulgar de la especie");
						auxEspecie.setDenominacionVulgar(lector2.nextLine());

						if (auxEspecie instanceof Animal) {
							do {
								bandera = true;
								System.out.println(
										"Ingrese tipo de alimentacion del animal (Carnivoro, Herbivoro, Omnivoro)");
								String aux = lector2.nextLine();
								if (aux.equalsIgnoreCase("carnivoro")) {
									bandera = true;
									((Animal) auxEspecie).setTipoAlimentacion("Carnivoro");

								} else if (aux.equalsIgnoreCase("herbivoro")) {
									bandera = true;
									((Animal) auxEspecie).setTipoAlimentacion("Herbivoro");

								} else if (aux.equalsIgnoreCase("omnivoro")) {
									bandera = true;
									((Animal) auxEspecie).setTipoAlimentacion("Omnivoro");

								} else {
									System.out.println("No se ingreso un tipo de alimentacion valido");
									bandera = false;
								}
							} while (bandera == false);
							System.out.println("Ingrese periodo de celo");
							((Animal) auxEspecie).setPeriodoCelo(lector2.nextLine());
							System.out.println("Esta en peligro de extincion ?");
							((Animal) auxEspecie).setPeligroExtincion(lector2.nextLine().equalsIgnoreCase("si"));
							TablaAnimales.animales.add((Animal) auxEspecie);
						}
						if (auxEspecie instanceof Vegetal) {
							System.out.println("Tiene floracion? ");
							((Vegetal) auxEspecie).setFloracion(lector2.nextLine().equalsIgnoreCase("si"));

							if (((Vegetal) auxEspecie).isFloracion() == true) {

								System.out.println("Ingrese periodo de floracion ");
								((Vegetal) auxEspecie).setPeriodo(lector2.nextLine());
							} else {
								((Vegetal) auxEspecie).setPeriodo("No tiene Floracion.");
							}
							TablaVegetales.vegetales.add((Vegetal) auxEspecie);
						}
						if (auxEspecie instanceof Mineral) {
							do {
								bandera = true;
								System.out.println("Ingrese tipo de mineral (Cristal o Piedra");
								String aux = lector2.next();
								if (aux.equalsIgnoreCase("cristal")) {
									((Mineral) auxEspecie).setTipoMineral("Cristal");
									bandera = true;
								} else if (aux.equalsIgnoreCase("piedra")) {
									((Mineral) auxEspecie).setTipoMineral("Piedra");
									bandera = true;
								} else {
									System.out.println("No se ingreso un tipo adecuado");
									bandera = false;
								}
							} while (bandera == false);
							TablaMinerales.minerales.add((Mineral) auxEspecie);

						}
						auxParque.getEspecies().add(auxEspecie);

						System.out.println("Desea seguir agregando especies ? (si/no)");
						respuesta = lector2.next();
					} while (respuesta.equalsIgnoreCase("si"));
				}

				do {
					System.out.println("Ingrese el nombre del organismo que lo maneja");
					String aux = lector2.nextLine();
					auxOrganismo = recorrerOrganismos(aux);
					if (auxOrganismo == null) {
						System.out.println("No existe tal organismo");
						bandera = false;
					} else
						bandera = true;
				} while (bandera == false);

				auxParque.setOrganismo(auxOrganismo);

				unAdministrativo.darDeAltaParque(auxParque);
				
				break;

			case 2:
				System.out.println("Ingrese Iso de la provincia:");
				auxProvincia.setIsoProvincia(lectorAdministrativo.next());
				String aux = "0";
				lectorAdministrativo.nextLine();
				do {
					bandera = true;
					System.out.println("Ingrese nombre de la provincia:");
					aux = lectorAdministrativo.nextLine();
					if (recorrerProvincias(aux) == true) {
						System.out.println("Ya existe una provincia con ese nombre");
						bandera = false;
					}

				} while (bandera == false);
				auxProvincia.setNombre(aux);
				
				unAdministrativo.darDeAltaProvincia(auxProvincia);
				
				break;

			case 3:
				Scanner lector3 = new Scanner(System.in);

				do {
					bandera = true;
				System.out.println("Ingrese nombre del organismo: ");
				
				aux = lector3.nextLine();
				 if (recorrerOrganismos(aux)!=null)
				 {
					 System.out.println("El Organismo ya existe");
					 bandera=false;
				 }
				
				}while(bandera==false);
		
				auxOrganismo.setNombre(aux);
				
				System.out.println("Ingrese direcion del organismo: ");
				auxOrganismo.setDireccion(lector3.nextLine());

				System.out.println("Ingrese director del organismo: ");
				auxOrganismo.setDirector(lector3.nextLine());

				do {
					bandera = false;
					System.out.println("Ingrese provincia a la cual pertenece: ");
					String auxNombreProvincia = lector3.nextLine();
					Provincia auxP = new Provincia();

					for (Provincia pr : TablaProvincias.provincias) {
						if (auxNombreProvincia.equals(pr.getNombre())) {
							auxP = pr;
							auxOrganismo.setProvincia(auxP);
							bandera = true;
							break;
						}
					}
				} while (bandera == false);
				
				unAdministrativo.darDeAltaOrganismo(auxOrganismo);
				
				break;

			}
		} while (opc != 4);
	lectorAdministrativo.close();	
	}

	public static boolean buscarVegetal(int codigo) {
		for (int i = 0; i < TablaVegetales.vegetales.size(); i++) {
			if (TablaVegetales.vegetales.get(i).getCodigo() == codigo) {
				return true;
			}

		}
		return false;
	}

	public static boolean buscarAnimal(int codigo) {
		for (int i = 0; i < TablaAnimales.animales.size(); i++) {
			if (TablaAnimales.animales.get(i).getCodigo() == codigo) {
				return true;
			}

		}
		return false;
	}

	public static boolean buscarMineral(int codigo) {
		for (int i = 0; i < TablaMinerales.minerales.size(); i++) {
			if (TablaMinerales.minerales.get(i).getCodigo() == codigo) {
				return true;
			}

		}
		return false;
	}

	public static LocalDate convertirStringLocalDate(String fecha) throws Exception {
		DateTimeFormatter formato = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate fechaLocalDate;
		try {
			fechaLocalDate = LocalDate.parse(fecha, formato);
		} catch (DateTimeParseException dtpe) {
			throw new Exception("La fecha ingresada no tiene formato de fecha");
		}

		return fechaLocalDate;
	}

	public static OrganismoProvincial recorrerOrganismos(String nombre) {
		for (int i = 0; i < TablaOrganismos.organismos.size(); i++) {
			if (TablaOrganismos.organismos.get(i).getNombre().equals(nombre)) {
				return TablaOrganismos.organismos.get(i);
			}
		}
		return null;
	}

	public static boolean recorrerProvincias(String nombre) {
		for (int i = 0; i < TablaProvincias.provincias.size(); i++) {
			if (TablaProvincias.provincias.get(i).getNombre().equals(nombre)) {
				return true;
			}

		}
		return false;
	}
}
