package ar.edu.unju.escminas.poo.dominio;

import java.time.LocalDate;
import java.util.Collections;

import ar.edu.unju.escminas.poo.tablas.TablaOrganismos;
import ar.edu.unju.escminas.poo.tablas.TablaParques;
import ar.edu.unju.escminas.poo.tablas.TablaProvincias;

public class Secretario extends Usuario{

	public Secretario() {
		// TODO Auto-generated constructor stub
	}
	
	public Secretario(int codigo, String usuario, String contraseņa, String nombre, String apellido, int dni,
			String tipoUsuario) {
		super(codigo, usuario, contraseņa, nombre, apellido, dni, tipoUsuario);
		// TODO Auto-generated constructor stub
	}


	public void listarParquesPorProvincia(String nombreProvincia) {
		for (Provincia pro : TablaProvincias.provincias) {
			if (nombreProvincia.equals(pro.getNombre())) {
				pro.getParques().forEach(e -> {
					if (e.getUbicacionGeografica().equals(nombreProvincia)) {
						System.out.println("Codigo: " + e.getCodigo());
						System.out.println("Nombre: " + e.getNombre());
						System.out.println("Extension: " + e.getExtension());
						System.out.println("Fecha declaracion: " + e.getFecha().toString());
						System.out.println("Organismo que lo maneja: " + e.getOrganismo());
						System.out.println(" ");
					}
				});
			}
		}
	}
	
	
	public void listarEspecies(Integer nombreParque) {
		int contA = 0, contM = 0, contV = 0;
		for (Parque aParque : TablaParques.parques) {
			if (aParque.getCodigo() == nombreParque) {
				for (Especie e : aParque.getEspecies()) {
					if (e instanceof Animal) {
						Animal animal = (Animal) e;
						System.out.println(animal);
						System.out.println(" ");
						contA += 1;

					}
					if (e instanceof Vegetal) {

						Vegetal vegetal = (Vegetal) e;
						System.out.println(vegetal);
						System.out.println(" ");
						contV += 1;
					}
					if (e instanceof Mineral) {

						Mineral mineral = (Mineral) e;
						System.out.println(mineral);
						System.out.println(" ");
						contM += 1;
					}
				}

				System.out.println("cantidad de animales = " + contA);
				System.out.println("cantidad de vegetales = " + contV);
				System.out.println("cantidad de minerales = " + contM);
			}
		}
	}
	
	public void listarParques10Anios() {
		for (int i = 0; i < TablaParques.parques.size(); i++) {
			if (LocalDate.now().getYear() >= (TablaParques.parques.get(i).getFecha().getYear() + 10)) {
				System.out.println(TablaParques.parques.get(i).toString());
			}
		}
	}
	
	
	public void listarOrganismosAlfabeticamente() {
		Collections.sort(TablaOrganismos.organismos,
				(elemento1, elemento2) -> elemento1.getNombre().compareTo(elemento2.getNombre()));
		for (OrganismoProvincial org : TablaOrganismos.organismos) {
			System.out.println(org.getNombre());
		}
	}

	@Override
	public String toString() {
		return "Secretario [toString()=" + super.toString() + "]";
	}
	
	
	
}
