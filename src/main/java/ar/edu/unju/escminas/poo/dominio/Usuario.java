package ar.edu.unju.escminas.poo.dominio;

public class Usuario {

	private int codigo;
	private String usuario;
	private String contraseña;
	private String nombre;
	private String apellido;
	private int dni;
	private String tipoUsuario;

	// constructores

	public Usuario(int codigo, String usuario, String contraseña, String nombre, String apellido, int dni,
			String tipoUsuario) {
		super();
		this.codigo = codigo;
		this.usuario = usuario;
		this.contraseña = contraseña;
		this.nombre = nombre;
		this.apellido = apellido;
		this.dni = dni;
		this.tipoUsuario = tipoUsuario;
	}

	public Usuario() {
		super();
	}

	// setters and getters

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public String getTipoUsuario() {
		return tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	@Override
	public String toString() {
		return "Usuario [codigo=" + codigo + ", usuario=" + usuario + ", contraseña=" + contraseña + ", nombre="
				+ nombre + ", apellido=" + apellido + ", dni=" + dni + ", tipoUsuario=" + tipoUsuario + "]";
	}

}
